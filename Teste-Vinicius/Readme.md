# Git Launch App

### Sympla Challenge 

##### Preview: 

![mockup](https://user-images.githubusercontent.com/32227073/67043334-356ada80-f100-11e9-8bcd-2fb54cc80be9.png)


##### Arquitetura: MVVM

###### Scheme
- Constructor
  - Camada de construção da aplicação
- Plugins
  - Network, Biblieotecas 
- Features
  - Controllers, Views, ViewModels.
- Entities
  - Modelos génericos reutilizáveis em toda aplicação
- ReusableLayer
  - Protocolos, Views reusáveis 
- AppTest
  - Unit Tests para cada feature

#### Features
- ListUsers
  - List usuários do GitHub com opção de busca
- InsideUser
  - Mostra detalhes do usuário, como bio, followers, following e repositórios
- InsideRepo
  - Mostra detalhes do repositório, como stars, forks, linguagem, descrição e colaboradores


##### Dependências

 - [SDWebImage](https://github.com/SDWebImage/SDWebImage) Autor: (SDWebImage) Para cache de imagem
 - [CBuilder](https://github.com/ViniciusDeep/CBuilder) Autor: (Vinicius Mangueira) Para facilitar o uso de constraints prográmaticas
 - [Providey](https://github.com/ViniciusDeep/Providey) Autor: (Vinicius Mangueira) Trabalhar com HTTP requests
 - [OTTHPStubs](https://github.com/AliSoftware/OHHTTPStubs) Autor: (Ali Software) Criar stubs para testes

### Install
#### Develop
#### iOS
1. `Clone project`
2. `$ pod install` 
3. Abra seu Xcode, selecione o simulador desejado, clique em run ou utilize `cmd + R`
